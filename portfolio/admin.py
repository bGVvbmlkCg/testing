from django.contrib import admin

from .models import Post, Category, Tag, Comment, Portfolio, ContactForm, Skill, Meta

admin.site.register(Post)
admin.site.register(Category)
admin.site.register(Tag)
admin.site.register(Comment)
admin.site.register(Portfolio)


@admin.register(ContactForm)
class ContactFormAdmin(admin.ModelAdmin):
    pass

@admin.register(Skill)
class SkillFormAdmin(admin.ModelAdmin):
    list_display = ('title','value')
    list_display_links = ('title',)
    list_editable = ('value',)

@admin.register(Meta)
class MetaAdmin(admin.ModelAdmin):
    list_display = ('key', 'value')
    list_editable = ('value',)

