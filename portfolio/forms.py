from django import forms
from .models import ContactForm
from django.contrib.auth.forms import UserCreationForm


# class ContactFormHTML(forms.Form):
#     name = forms.CharField(max_length=100, widget=forms.TextInput(attrs={"class":"form-control"}))
#     email = forms.EmailField(widget=forms.EmailInput(attrs={"class":"form-control"}))
#     subject = forms.CharField(max_length=400, widget=forms.TextInput(attrs={"class":"form-control"}))
#     message = forms.CharField(widget=forms.Textarea(attrs={"class":"form-control"}))

class ContactFormHTML(forms.ModelForm):
    class Meta:
        model = ContactForm
        fields = "__all__"
        widgets = {
            "name": forms.TextInput(attrs={"class": "form-control", "placeholder": "Name"}),
            "email": forms.EmailInput(attrs={"class": "form-control", "placeholder": "Email"}),
            "subject": forms.TextInput(attrs={"class": "form-control", "placeholder": "Subject"}),
            "message": forms.Textarea(attrs={"class": "form-control", "placeholder": "Message"}),
        }

class UserCreationFormWithoutHelpText(UserCreationForm):
    def __init__(self):
        super(UserCreationFormWithoutHelpText, self). __init__()
        for fieldname in ['username', 'password1', 'password2']:
            self.fields[fieldname].help_text = None
