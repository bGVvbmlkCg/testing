from django.urls import path
from .views import homepage, print_context, BlogSingle, Login, Register

urlpatterns = [
    path('', homepage),
    path('context/', print_context),
    path('blog/<int:pk>', BlogSingle.as_view(), name="blog_single"),
    path('login/', Login.as_view(), name='login'),
    path('register/', Register.as_view(), name='register'),

]